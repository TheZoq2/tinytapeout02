# top=main::layer_test_harness

import sys
import os
sys.path.append(os.getcwd() + "/../../test")
print("IMPORT PATH: ", sys.path)

from typing import List
from spade import *

from cocotb.clock import Clock;


from util.signal_gen import config_cell, config_layer
from util.config import str_to_bits
from util import bitstr_to_spade

def set_layer_inputs(s, inputs: List[str]):
    assert len(inputs) == 2
    assert len(inputs[0]) == 4
    assert len(inputs[1]) == 4

    s.i.cell_out = bitstr_to_spade(inputs[0])
    s.i.feedback = bitstr_to_spade(inputs[1])

async def layer_test(dut, cfg, stimuli):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units='ns').start())

    # Config chain clock
    cfg_clk = dut.cfg_clk_i
    cfg_handle = await cocotb.start(Clock(cfg_clk, 3, units='ns').start())

    await FallingEdge(cfg_clk)
    await config_layer(cfg_clk, s, cfg)

    # We're done configuring, stop the config clock
    cfg_handle.kill()

    # Make sure the config chain doesn't do anything spooky after configuration
    await Timer(10, units='ns')
    await FallingEdge(clk)

    for stim in stimuli:
        # 0, 1, ... produces high output
        set_layer_inputs(s, stim[0])

        await Timer(1, units='ps')
        assert len(stim[1]) == 4
        s.o.assert_eq(bitstr_to_spade(stim[1]))



# 3 and gates taking different inputs
@cocotb.test()
async def and_gates_with_different_inputs(dut):
    await layer_test(
        dut,
        [
            [ "1000", "000", "001"],
            [ "1000", "010", "011"],
            [ "1000", "100", "101"]
        ],
        [
            (["1100", "0000"], "1000"),
            (["0100", "0000"], "0000"),
            (["1000", "0000"], "0000"),
            (["0000", "0000"], "0000"),

            (["0011", "0000"], "0100"),
            (["0001", "0000"], "0000"),
            (["0010", "0000"], "0000"),
            (["0000", "0000"], "0000"),

            (["0000", "1100"], "0010"),
            (["0000", "0100"], "0000"),
            (["0000", "1000"], "0000"),
            (["0000", "0000"], "0000"),
        ]
    )


