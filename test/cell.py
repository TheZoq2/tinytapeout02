# top=cell::cell_test_harness

import sys
import os
sys.path.append(os.getcwd() + "/../../test")
print("IMPORT PATH: ", sys.path)

from util.config import str_to_bits, cell_bitstream
from util.signal_gen import config_cell, send_bitstream

from typing import List
from spade import *

from cocotb.clock import Clock;

async def check_output(s, x, y, expected, trigger):
    s.i.x = x
    s.i.y = y
    await trigger
    s.o.assert_eq(expected)


async def setup_test(dut, lut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units='ns').start())

    # Config chain clock
    cfg_clk = dut.cfg_clk_i
    cfg_handle = await cocotb.start(Clock(cfg_clk, 3, units='ns').start())

    await FallingEdge(cfg_clk)
    await config_cell(cfg_clk, s, str_to_bits(lut))

    # We're done configuring, stop the config clock
    cfg_handle.kill()

    # Make sure the config chain doesn't do anything spooky after configuration
    await Timer(10, units='ns')
    await FallingEdge(clk)

    return (s, clk)

@cocotb.test()
async def lut_without_reg(dut):
    (s, clk) = await setup_test(dut, "1010")

    await check_output(s, "false", "false", "false", FallingEdge(clk))
    await check_output(s, "false", "true", "true", FallingEdge(clk))
    await check_output(s, "true", "false", "false", FallingEdge(clk))
    await check_output(s, "true", "true", "true", FallingEdge(clk))


@cocotb.test()
async def bit_0_is_as_expected(dut):
    (s, clk) = await setup_test(dut, "0001")

    await check_output(s, "false", "false", "true", FallingEdge(clk))
    await check_output(s, "false", "true", "false", FallingEdge(clk))
    await check_output(s, "true", "false", "false", FallingEdge(clk))
    await check_output(s, "true", "true", "false", FallingEdge(clk))

@cocotb.test()
async def bit_3_is_as_expected(dut):
    (s, clk) = await setup_test(dut, "1000")

    await check_output(s, "false", "false", "false", FallingEdge(clk))
    await check_output(s, "false", "true", "false", FallingEdge(clk))
    await check_output(s, "true", "false", "false", FallingEdge(clk))
    await check_output(s, "true", "true", "true", FallingEdge(clk))


@cocotb.test()
async def or_gate_works(dut):
    (s, clk) = await setup_test(dut, "1110")

    await check_output(s, "false", "false", "false", FallingEdge(clk))
    await check_output(s, "false", "true", "true", FallingEdge(clk))
    await check_output(s, "true", "false", "true", FallingEdge(clk))
    await check_output(s, "true", "true", "true", FallingEdge(clk))

@cocotb.test()
async def detect_01_works(dut):
    (s, clk) = await setup_test(dut, "0010")

    await check_output(s, "false", "false", "false", FallingEdge(clk))
    await check_output(s, "false", "true", "true", FallingEdge(clk))
    await check_output(s, "true", "false", "false", FallingEdge(clk))
    await check_output(s, "true", "true", "false", FallingEdge(clk))


@cocotb.test()
async def detect_10_works(dut):
    (s, clk) = await setup_test(dut, "0100")

    await check_output(s, "false", "false", "false", FallingEdge(clk))
    await check_output(s, "false", "true", "false", FallingEdge(clk))
    await check_output(s, "true", "false", "true", FallingEdge(clk))
    await check_output(s, "true", "true", "false", FallingEdge(clk))
