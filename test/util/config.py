from typing import List

import itertools

def str_to_bits(s: str):
    assert all(map(lambda c: c == '0' or c == '1', s))
    return [c == '1' for c in s]



def cell_bitstream(lut: List[bool]) -> List[bool]:
    assert len(lut) == 4
    return list(lut)


def cell_routing_bitstream(bits):
    assert len(bits) == 3
    return list(reversed(bits))


def feedback_tile_bitstream(bits: str):
    assert len(bits) == 4
    return list(reversed(str_to_bits(bits)))


def layer_bitstream(config: List[List[str]]):
    def cell_and_routing(cell: List[str]):
        # We expect cell config, y routing, x routing
        assert len(cell) == 3
        # Note: Inverse order 2,1 to get x,y in a natural order
        return cell_bitstream(str_to_bits(cell[0])) \
            + cell_routing_bitstream(str_to_bits(cell[2])) \
            + cell_routing_bitstream(str_to_bits(cell[1]))

    return list(itertools.chain.from_iterable(map(cell_and_routing, reversed(config))))


def fpga_bitstream(config):
    full_bistream = []
    for (type, cell) in reversed(config):
        if type == "layer":
            full_bistream += layer_bitstream(cell)
        elif type == "feedback":
            full_bistream += feedback_tile_bitstream(cell)
        else:
            raise Exception("Unknown cell type: " + type)
    return full_bistream
