from spade import *

from typing import List
from cocotb.clock import Clock;

from util.config import *

async def config_cell(cfg_clk, s, lut: List[bool]):
    await send_bitstream(cfg_clk, s, cell_bitstream(lut))


async def config_cell_routing(cfg_clk, s, bits: List[bool]):
    await send_bitstream(cfg_clk, s, cell_routing_bitstream(bits))


async def config_feedback_tile(cfg_clk, s, bits: str):
    await send_bitstream(cfg_clk, s, feedback_tile_bitstream(bits))


# Configures all the cells in a layer. Input is a bit stream of [cell(lut, x, y)...]
# The first input is the earliest cell in the chain
async def config_layer(cfg_clk, s, bits: List[List[str]]):
    assert len(bits) == 3
    await send_bitstream(cfg_clk, s, layer_bitstream(bits))


# Bitstream generation
async def send_bitstream(cfg_clk, s, bits: List[bool]):
    for b in bits:
        s.i.cfg_value = f"{b}".lower()
        await FallingEdge(cfg_clk)
