from typing import List

def bool_array_to_spade(bits: List[bool]) -> str:
    inner = ",".join([f"{b}" for b in bits]).lower()
    return f"[{inner}]"

# Converts "0101" to "[false, true, false, true]
def bitstr_to_spade(bits: str) -> str:
    if not isinstance(bits, str):
        raise Exception("bitstr_to_spade of non-string")
    inner = ",".join([f"{c == '1'}" for c in bits]).lower()
    return f"[{inner}]"
