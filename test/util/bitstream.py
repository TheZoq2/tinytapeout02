
def all_ones():
    feedback = ("feedback", "1111") # Feed bits from the previous layer
    config = [
        ("layer", [
            [ "1111", "111", "111"], [ "1111", "111", "111"], [ "1111", "111", "111"]
        ]),
        feedback,
        ("layer", [
            [ "1111", "111", "111"], [ "1111", "111", "111"], [ "1111", "111", "111"]
        ]),
        feedback,
        ("layer", [
            [ "1111", "111", "111"], [ "1111", "111", "111"], [ "1111", "111", "111"]
        ]),
        feedback,
    ]

    return config

def three_way_and_cfg():
    feedback = ("feedback", "0000") # Feed bits from the previous layer
    config = [
        # Mux 1 and 2 are and gates of inputs (0 & 1) and (2 & 3)
        ("layer", [
            [ "1000", "000", "001"], [ "1000", "010", "011"], [ "0000", "000", "000"]
        ]),
        feedback,
        ("layer", [
            # The next layer is an and gate of the previous layer bit 0 and 1
            [ "1000", "000", "001"], [ "0000", "000", "000"], [ "0000", "000", "000"]
        ]),
        feedback,
        # The third layer is just a feed through
        ("layer", [
            # Feed through inputs
            [ "1100", "000", "000"], [ "1100", "001", "000"], [ "1100", "010", "000"]
        ]),
        feedback,
    ]

    return config


def double_and_cfg():
    feedback = ("feedback", "0000") # Feed bits from the previous layer
    config = [
        # Mux 1 and 2 are and gates of inputs (0 & 1) and (2 & 3)
        ("layer", [
            [ "1000", "000", "001"], [ "1000", "010", "011"], [ "0000", "000", "000"]
        ]),
        feedback,
        ("layer", [
            # Feed through inputs
            [ "1100", "000", "000"], [ "1100", "001", "001"], [ "1100", "010", "000"]
        ]),
        feedback,
        # The third layer is just a feed through
        ("layer", [
            # Feed through inputs
            [ "1100", "000", "000"], [ "1100", "001", "001"], [ "1100", "010", "000"]
        ]),
        feedback,
    ]

    return config


def four_way_or_cfg():
    feedback = ("feedback", "0000") # Feed bits from the previous layer
    config = [
        # Mux 1 and 2 are and gates of inputs (0 & 1) and (2 & 3)
        ("layer", [
            [ "1110", "000", "001"], [ "1110", "010", "011"], [ "0000", "000", "000"]
        ]),
        feedback,
        ("layer", [
            # The next layer is an and gate of the previous layer bit 0 and 1
            [ "1110", "000", "001"], [ "0000", "000", "000"], [ "0000", "000", "000"]
        ]),
        feedback,
        # The third layer is just a feed through
        ("layer", [
            # Feed through inputs
            [ "1100", "000", "000"], [ "1100", "001", "000"], [ "1100", "010", "000"]
        ]),
        feedback,
    ]

    return config


def change_detector_cfg():
    feedback = ("feedback", "0000") # Feed bits from the previous layer
    config = [
        # LUT 1 is a feed through of input 0 to store it for later
        # LUT 0 is an XOR between the stored value and the new value
        ("layer", [
            [ "0110", "000", "101"], [ "1100", "000", "000"], [ "0000", "000", "000"]
        ]),
        feedback,
        # The rest are just feedthrough
        ("layer", [
            # Feed through 0, set rest to 0
            [ "1100", "000", "000"], [ "1100", "001", "000"], [ "1100", "010", "000"]
        ]),
        feedback,
        # The third layer is just a feed through
        ("layer", [
            # Feed through inputs
            [ "1100", "000", "000"], [ "1100", "001", "000"], [ "1100", "010", "000"]
        ]),
        feedback,
    ]

    return config


def toggle_button_cfg():
    feedback = ("feedback", "0000") # Feed bits from the previous layer
    config = [
        # LUT 1 is a feed through of input 0 to store it for later
        # LUT 0 is an XOR between the stored value and the new value
        ("layer", [
            [ "0110", "000", "101"], [ "1100", "000", "000"], [ "0000", "000", "000"]
        ]),
        feedback,
        # LUT0 is XOR of current value and LUT0 from before, a toggle 
        ("layer", [
            # Feed through 0, set rest to 0
            [ "0110", "000", "100"], [ "1100", "001", "000"], [ "1100", "010", "000"]
        ]),
        feedback,
        # The third layer is just a feed through
        ("layer", [
            # Feed through inputs
            [ "1100", "000", "000"], [ "1100", "001", "000"], [ "1100", "010", "000"]
        ]),
        feedback,
    ]

    return config
