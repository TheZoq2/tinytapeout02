# top=routing::feedback_tile_test_harness

import sys
import os
sys.path.append(os.getcwd() + "/../../test")
print("IMPORT PATH: ", sys.path)

from util.signal_gen import config_feedback_tile
from util import bitstr_to_spade

from typing import List
from spade import *

from cocotb.clock import Clock;

async def setup_tile(dut, config):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units='ns').start())

    # Config chain clock
    cfg_clk = dut.cfg_clk_i
    cfg_handle = await cocotb.start(Clock(cfg_clk, 3, units='ns').start())

    await FallingEdge(cfg_clk)
    # Only use feedback values
    await config_feedback_tile(cfg_clk, s, config)

    # We're done configuring, stop the config clock
    cfg_handle.kill()

    # Make sure the config chain doesn't do anything spooky after configuration
    await Timer(10, units='ns')
    await FallingEdge(clk)

    return (s, clk)


async def set_inputs(s, from_layer, from_previous):
    s.i.from_layer = bitstr_to_spade(from_layer)
    s.i.from_previous = bitstr_to_spade(from_previous)


@cocotb.test()
async def only_from_layer(dut):
    (s, clk) = await setup_tile(dut, "0000")

    # Output updates after one cycle
    await set_inputs(s, "1111", "0000")
    await FallingEdge(clk)
    s.o.assert_eq(bitstr_to_spade("1111"))

    # There are FFs on the from_layer inputs
    await set_inputs(s, "1010", "0000")
    await Timer(1, units='ps')
    s.o.assert_eq(bitstr_to_spade("1111"))
    await FallingEdge(clk)
    s.o.assert_eq(bitstr_to_spade("1010"))

@cocotb.test()
async def only_from_previous(dut):
    (s, clk) = await setup_tile(dut, "1111")

    # Output updates after one cycle
    await set_inputs(s, "1111", "0000")
    await FallingEdge(clk)
    s.o.assert_eq(bitstr_to_spade("0000"))

    # There are no FFs on the from_previous inputs
    await set_inputs(s, "1111", "0101")
    await Timer(1, units='ps')
    s.o.assert_eq(bitstr_to_spade("0101"))
    await FallingEdge(clk)
    s.o.assert_eq(bitstr_to_spade("0101"))

async def quick_test(dut, config, input, output):
    (s, clk) = await setup_tile(dut, "1111")

    # Output updates after one cycle
    await set_inputs(s, input[0], input[1])
    await FallingEdge(clk)
    s.o.assert_eq(bitstr_to_spade(output))

@cocotb.test()
async def one_bit_is_fed_through_1(dut):
    await quick_test(dut, "1000", ["0000", "1000"], "1000")

@cocotb.test()
async def only_from_previous(dut):
    await quick_test(dut, "0100", ["0000", "0100"], "0100")
