# top=main::cell_test

import sys
import os
sys.path.append(os.getcwd() + "/../../test")
print("IMPORT PATH: ", sys.path)

from util import bitstr_to_spade
from util.signal_gen import send_bitstream
from util.config import fpga_bitstream
from util.bitstream import three_way_and_cfg, change_detector_cfg, toggle_button_cfg

from typing import List
from spade import *

from cocotb.clock import Clock;

async def setup_test(dut, config):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units='ns').start())

    # Config chain clock
    cfg_clk = dut.cfg_clk_i
    cfg_handle = await cocotb.start(Clock(cfg_clk, 3, units='ns').start())

    await FallingEdge(cfg_clk)
    await send_bitstream(cfg_clk, s, fpga_bitstream(config))

    # We're done configuring, stop the config clock
    cfg_handle.kill()

    # Make sure the config chain doesn't do anything spooky after configuration
    await Timer(10, units='ns')
    await FallingEdge(clk)

    return (s, clk)



@cocotb.test()
async def constant_111_output(dut):

    layer = ("layer", [
            # Feed through the x signal. Use X and Y signnals from input layer
            [ "1111", "000", "001"], [ "1111", "000", "001"], [ "1111", "000", "001"]
        ])
    feedback = ("feedback", "0000") # Feedack is unimportant here
    config = [
        layer,
        feedback,
        layer,
        feedback,
        layer,
        feedback,
    ]

    (s, clk) = await setup_test(dut, config)

    s.i.inputs = bitstr_to_spade("0000")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_1110")


@cocotb.test()
async def constant_001_output(dut):

    layer = ("layer", [
            # Feed through the x signal. Use X and Y signnals from input layer
            [ "0000", "000", "001"], [ "0000", "000", "001"], [ "1111", "000", "001"]
        ])
    feedback = ("feedback", "0000") # Feedack is unimportant here
    config = [
        layer,
        feedback,
        layer,
        feedback,
        layer,
        feedback,
    ]

    (s, clk) = await setup_test(dut, config)

    s.i.inputs = bitstr_to_spade("0000")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0010")


@cocotb.test()
async def constant_010_output(dut):

    layer = ("layer", [
            # Feed through the x signal. Use X and Y signnals from input layer
            [ "0000", "000", "001"], [ "1111", "000", "001"], [ "0000", "000", "001"]
        ])
    feedback = ("feedback", "0000") # Feedack is unimportant here
    config = [
        layer,
        feedback,
        layer,
        feedback,
        layer,
        feedback,
    ]

    (s, clk) = await setup_test(dut, config)

    s.i.inputs = bitstr_to_spade("0000")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0100")

@cocotb.test()
async def constant_100_output(dut):

    layer = ("layer", [
            # Feed through the x signal. Use X and Y signnals from input layer
            [ "1111", "000", "001"], [ "0000", "000", "001"], [ "0000", "000", "001"]
        ])
    feedback = ("feedback", "0000") # Feedack is unimportant here
    config = [
        layer,
        feedback,
        layer,
        feedback,
        layer,
        feedback,
    ]

    (s, clk) = await setup_test(dut, config)

    s.i.inputs = bitstr_to_spade("0000")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_1000")


@cocotb.test()
async def constant_100_output_on_last_layer_only(dut):

    constant_1 = ("layer", [
            # Feed through the x signal. Use X and Y signnals from input layer
            [ "1111", "000", "001"], [ "0000", "000", "001"], [ "0000", "000", "001"]
        ])
    constant_0 = ("layer", [
            # Feed through the x signal. Use X and Y signnals from input layer
            [ "0000", "000", "001"], [ "0000", "000", "001"], [ "0000", "000", "001"]
        ])
    feedback = ("feedback", "0000") # Feedack is unimportant here
    config = [
        constant_0,
        feedback,
        constant_0,
        feedback,
        constant_1,
        feedback,
    ]

    (s, clk) = await setup_test(dut, config)

    s.i.inputs = bitstr_to_spade("0000")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_1000")


@cocotb.test()
async def o0_is_i0(dut):

    layer = ("layer", [
            # Feed through inputs, the leftmost lut will have the input propagated
            # through it
            [ "1100", "000", "001"], [ "0000", "000", "000"], [ "0000", "000", "001"]
        ])
    feedback = ("feedback", "0000") # Feedack is unimportant here
    config = [
        layer,
        feedback,
        layer,
        feedback,
        layer,
        feedback,
    ]

    (s, clk) = await setup_test(dut, config)

    s.i.inputs = bitstr_to_spade("1000")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_1000")

    s.i.inputs = bitstr_to_spade("0000")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0000")

    # Driving the adjacent bit high does not change the result
    s.i.inputs = bitstr_to_spade("1100")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_1000")

    s.i.inputs = bitstr_to_spade("0100")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0000")

@cocotb.test()
async def o1_is_i1(dut):

    layer = ("layer", [
            # Feed through inputs
            [ "0000", "000", "000"], [ "1100", "001", "010"], [ "0000", "000", "000"]
        ])
    feedback = ("feedback", "0000") # Feedack is unimportant here
    config = [
        layer,
        feedback,
        layer,
        feedback,
        layer,
        feedback,
    ]

    (s, clk) = await setup_test(dut, config)

    s.i.inputs = bitstr_to_spade("0100")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0100")

    s.i.inputs = bitstr_to_spade("0000")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0000")


@cocotb.test()
async def three_way_and_works(dut):
    config = three_way_and_cfg()

    (s, clk) = await setup_test(dut, config)

    s.i.inputs = bitstr_to_spade("1111")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_1000")

    s.i.inputs = bitstr_to_spade("1010")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0000")

    s.i.inputs = bitstr_to_spade("1100")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0000")

    s.i.inputs = bitstr_to_spade("0110")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0000")

    s.i.inputs = bitstr_to_spade("0010")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0000")

    s.i.inputs = bitstr_to_spade("0100")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0000")

    s.i.inputs = bitstr_to_spade("1000")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0000")


    s.i.inputs = bitstr_to_spade("0000")
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_0000")


@cocotb.test()
async def change_detector(dut):
    config = change_detector_cfg()

    (s, clk) = await setup_test(dut, config)

    s.i.inputs = bitstr_to_spade("0000")
    # Flush the FFs, Let the input settle
    await FallingEdge(clk)
    await FallingEdge(clk)
    s.o.assert_eq("0b0000_0000")

    s.i.inputs = bitstr_to_spade("1000")
    # We have no synchronisation, so output shold propagate right away
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_1100")
    await FallingEdge(clk)
    s.o.assert_eq("0b0000_0100")

    # After another cycle there should be no more falling edges
    await FallingEdge(clk)
    s.o.assert_eq("0b0000_0100")


@cocotb.test()
async def toggle_button(dut):
    config = toggle_button_cfg()

    (s, clk) = await setup_test(dut, config)

    s.i.inputs = bitstr_to_spade("0000")
    # Flush the FFs, Let the input settle
    await FallingEdge(clk)
    await FallingEdge(clk)
    s.o.assert_eq("0b0000_0000")

    s.i.inputs = bitstr_to_spade("1000")
    # We have no synchronisation, so output shold propagate right away
    await Timer(1, units='ps')
    s.o.assert_eq("0b0000_1100")
    await FallingEdge(clk)
    s.o.assert_eq("0b0000_1100")

    # After another cycle there should be no more falling edges
    await FallingEdge(clk)
    s.o.assert_eq("0b0000_1100")
