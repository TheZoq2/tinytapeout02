import argparse
import sys
import os
sys.path.append(os.getcwd() + "/../test")

from util.config import fpga_bitstream
from util.bitstream import *
from util import bool_array_to_spade

def main(template_file, target_file):
    bitstream = fpga_bitstream(all_ones())
    # bitstream = fpga_bitstream(four_way_or_cfg())
    # bitstream = fpga_bitstream(double_and_cfg())
    # bitstream = fpga_bitstream(change_detector_cfg())
    # bitstream = fpga_bitstream(toggle_button_cfg())

    with open(template_file) as f:
        template = f.read()

    new_file = template \
        .replace("$BITSTREAM_LEN$", str(len(bitstream))) \
        .replace("$BITSTREAM$", bool_array_to_spade(bitstream))

    with open(target_file, "w") as f:
        f.write(new_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('template', type=str)
    parser.add_argument('target', type=str)

    args = parser.parse_args()
    main(template_file = args.template, target_file = args.target)
