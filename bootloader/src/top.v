module top
    ( input clk
    , output cfg_clk
    , output cfg_val
    );

    reg rst = 1;
    always @(posedge clk) begin
        rst <= 0;
    end

    main main
        ( .clk_i(clk)
        , .rst_i(rst)
        , .output__({cfg_clk, cfg_val})
        );
endmodule
