FROM efabless/openlane:2022.07.02_01.38.08

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y
RUN yum group install "Development Tools" -y
RUN yum install centos-release-scl -y
RUN yum install devtoolset-11 -y
RUN yum install -y tcl-devel readline-devel libffi-devel

COPY build/spade build/spade
COPY plugins/yosys_flamegraph plugins/yosys_flamegraph
RUN echo ""

RUN ls
RUN pwd
RUN ls plugins/yosys_flamegraph

WORKDIR plugins/yosys_flamegraph/swim_flamegraph
RUN ln -fs ../../../build/spade spade
RUN scl enable devtoolset-11 'source $HOME/.cargo/env && cargo build --release'
WORKDIR ../
RUN ls swim_flamegraph
RUN scl enable devtoolset-11 'make BUILD_DIR=/flamegraph_build IS_SPADE=1'
