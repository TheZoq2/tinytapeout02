## Local builds

Pushing to github for every change to annoying fast, so here are some steps
to do the build locally.

### Setup

Caravel requires docker, but docker requires superuser permissions. You can, however,
replace docker with podman. To do so:

```
systemctl enable --now --user podman.socket
export DOCKER_HOST=unix://$XDG_RUNTIME_DIR/podman/podman.sock
```

Set some environment variables. Change the paths to your liking

```
export OPENLANE_ROOT=~/build/caravel_user_project/build/openlane
export PDK_ROOT=~/build/caravel_user_project/build/pdk
export PDK=sky130B
export OPENLANE_IMAGE_NAME=efabless/openlane:2022.07.02_01.38.08
```

Then clone and build caravel

```
git clone https://github.com/efabless/caravel_user_project.git -b mpw-7a
cd caravel_user_project
make setup
```

### Build

To build the project, run:
```
./configure.py --create-user-config


podman run --rm \
    -v $OPENLANE_ROOT:/openlane \
    -v $PDK_ROOT:$PDK_ROOT \
    -v "$(pwd):/work" \
    -e PDK_ROOT=$PDK_ROOT \
    $OPENLANE_IMAGE_NAME \
    /bin/bash -c "./flow.tcl -verbose 2 -overwrite -design /work/src -run_path /work/runs -tag wokwi" \
```
