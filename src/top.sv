module top(input clk_25mhz, input[27:0] gn, input[6:0] btn, output[7:0] led);
    reg rst = 1;
    always @(posedge clk_25mhz) begin
        rst <= 0;
    end

    e_main main
        ( .clk_i(clk_25mhz)
        , .inputs_unsync_i(btn[6:3])
        , .output__(led)
        , .cfg_value_i(gn[21])
        , .cfg_clk_i(gn[22])
        );
endmodule

